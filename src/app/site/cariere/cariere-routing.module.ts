import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CariereComponent } from './cariere.component';

const routes: Routes = [{ path: '', component: CariereComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CariereRoutingModule { }
