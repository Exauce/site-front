import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CariereRoutingModule } from './cariere-routing.module';
import { CariereComponent } from './cariere.component';


@NgModule({
  declarations: [CariereComponent],
  imports: [
    CommonModule,
    CariereRoutingModule
  ]
})
export class CariereModule { }
