import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CariereComponent } from './cariere.component';

describe('CariereComponent', () => {
  let component: CariereComponent;
  let fixture: ComponentFixture<CariereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CariereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CariereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
