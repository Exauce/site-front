import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreventionComponent } from './prevention.component';

const routes: Routes = [{ path: '', component: PreventionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreventionRoutingModule { }
