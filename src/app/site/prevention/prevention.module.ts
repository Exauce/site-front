import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreventionRoutingModule } from './prevention-routing.module';
import { PreventionComponent } from './prevention.component';


@NgModule({
  declarations: [PreventionComponent],
  imports: [
    CommonModule,
    PreventionRoutingModule
  ]
})
export class PreventionModule { }
