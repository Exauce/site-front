import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import {Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class SiteComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  constructor(private router: Router) { }

  ngOnInit() {
    this.loadScriptsSite();
    this.subscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => window.scrollTo(0, 0));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  loadScriptsSite() {
    const dynamicScripts = [
      '../../assets/js/jquery-1.12.1.min.js',
      '../../assets/js/jquery-1.12.1.min.js',
      '../../assets/js/popper.min.js',
      '../../assets/js/bootstrap.min.js',
      '../../assets/js/jquery.magnific-popup.js',
      '../../assets/js/swiper.min.js',
      '../../assets/js/isotope.pkgd.min.js',
      '../../assets/js/owl.carousel.min.js',
      '../../assets/js/jquery.nice-select.min.js',
      '../../assets/js/slick.min.js',
      '../../assets/js/jquery.counterup.min.js',
      '../../assets/js/waypoints.min.js',
      '../../assets/js/custom.js'
    ];

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }

  }

}
