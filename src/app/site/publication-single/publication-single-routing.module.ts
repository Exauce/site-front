import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicationSingleComponent } from './publication-single.component';

const routes: Routes = [{ path: '', component: PublicationSingleComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationSingleRoutingModule { }
