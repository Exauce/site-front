import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationSingleComponent } from './publication-single.component';

describe('PublicationSingleComponent', () => {
  let component: PublicationSingleComponent;
  let fixture: ComponentFixture<PublicationSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
