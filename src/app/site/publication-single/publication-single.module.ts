import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicationSingleRoutingModule } from './publication-single-routing.module';
import { PublicationSingleComponent } from './publication-single.component';


@NgModule({
  declarations: [PublicationSingleComponent],
  imports: [
    CommonModule,
    PublicationSingleRoutingModule
  ]
})
export class PublicationSingleModule { }
