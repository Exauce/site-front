import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteRoutingModule } from './site-routing.module';
import { SiteComponent } from './site.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { ScrollToDirectiveDirective } from './menu/scroll-to-directive.directive';


@NgModule({
  declarations: [SiteComponent, MenuComponent, FooterComponent, ScrollToDirectiveDirective],
  imports: [
    CommonModule,
    SiteRoutingModule
  ]
})
export class SiteModule { }
