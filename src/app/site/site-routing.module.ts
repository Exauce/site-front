import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteComponent } from './site.component';

const routes: Routes = [{ path: '', component: SiteComponent,
                          children: [
                            { path: '', loadChildren: () => import('./accueil/accueil.module').then(m => m.AccueilModule) },
                            { path: 'contact', loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule) },
                            { path: 'devis', loadChildren: () => import('./devis/devis.module').then(m => m.DevisModule) },
                            { path: 'cariere', loadChildren: () => import('./cariere/cariere.module').then(m => m.CariereModule) },
                            { path: 'securite', loadChildren: () => import('./securite/securite.module').then(m => m.SecuriteModule) },
                            // tslint:disable-next-line:max-line-length
                            { path: 'prevention', loadChildren: () => import('./prevention/prevention.module').then(m => m.PreventionModule) },
                            // tslint:disable-next-line:max-line-length
                            { path: 'assistance', loadChildren: () => import('./assistance/assistance.module').then(m => m.AssistanceModule) },
                            { path: 'about', loadChildren: () => import('./about/about.module').then(m => m.AboutModule) },
                            // tslint:disable-next-line:max-line-length
                            { path: 'publications', loadChildren: () => import('./publications/publications.module').then(m => m.PublicationsModule) },
                            // tslint:disable-next-line:max-line-length
                            { path: 'publication-single', loadChildren: () => import('./publication-single/publication-single.module').then(m => m.PublicationSingleModule) }
                        ]
                        }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
