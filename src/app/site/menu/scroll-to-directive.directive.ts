import { Directive, HostListener, Input, OnInit} from '@angular/core';
declare var $: any;

@Directive({
  selector: '[appScrollToDirective]'
})
export class ScrollToDirectiveDirective implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('scrollTo') scrollTo: string;
  // tslint:disable-next-line:no-input-rename
  @Input('scrollBoxID') scrollBoxID: string;

  constructor() { }
  ngOnInit(): void {
  }

  @HostListener('mousedown')
  onMouseClick() {
      const id = this.scrollTo;
      const scrollBoxID = this.scrollBoxID;
      const elementOffset = $(scrollBoxID).offset().top + 10;

      $(scrollBoxID).animate({
          scrollTop: $(scrollBoxID).scrollTop() + ($(id).offset().top - elementOffset)
      }, 1000);
  }

}
